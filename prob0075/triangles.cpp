#include <fstream>
#include <iomanip>
#include <iostream>
#include <math.h>
#include <set>
#include <unordered_set>


template <class T>
T gcd(const T a, const T b) {
	return (a < b) ? gcd<T>(b, a) : ((a % b == 0) ? b : gcd<T>(b, a % b));
}


int main(int argc, const char *argv[]) {
	// std::unordered_set<uint32_t> lengths, rejects;
	std::set<uint32_t> lengths, rejects;


	const uint32_t max = 1500000;
	// const uint32_t max = 1000;
	const uint32_t max_m = std::sqrt<uint32_t>(max / 2);

	for (uint32_t m = 2; m <= max_m; m++) {
		if (2 * m * (m + 1) > max) {
			break;
		}
		for (uint32_t n = 1; n < m; n++) {
			if (gcd<uint32_t>(m, n) != 1 /* It must have come up earlier*/
			    || (m % 2 == 1 && n % 2 == 1)) {
				continue;
			}
			uint32_t perim = 2 * m * (m + n);
			if (perim > max) {
				break; // break innermost loop, since n will only keep increasing
			}

			for (uint32_t full_perim = perim; full_perim <= max;
			     full_perim += perim) {
				if (!lengths.insert(full_perim).second) {
					rejects.insert(full_perim);
				}
			}
		}
	}

	std::cout << "total: " << lengths.size() << '\n'
	          << "rejects: " << rejects.size() << '\n'
	          << "unique: " << lengths.size() - rejects.size() << std::endl;

	return 0;
}
