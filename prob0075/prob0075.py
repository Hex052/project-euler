import math

maxL = 1500000
L = set()
R = set()

m = 1
n = 1
k = 1
per = 0

justbroke = 0
endprgm = 0

for m in range(1,870):
    for n in range(1,m):
        if math.gcd(m,n)>1 or (m-n)%2==0:
            continue
        k = 1
        per = 2*m*(n+m)
        perk = per*1
        while perk<=maxL:
            if perk in L:
                R.add(perk)
            else:
                L.add(perk)
            k += 1
            perk = per*k

print(len(L)-len(R))
