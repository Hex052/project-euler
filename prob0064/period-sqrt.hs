module Main where

import Data.List (intercalate)
import Debug.Trace (trace)

isqrt = floor . sqrt . fromIntegral

squareRootWholeParts = map isqrt [0 .. 10000]
  where
    isqrt = floor . sqrt . fromIntegral

data ContinuedFraction = ContinuedFraction {whole :: Int, fractions :: [Int]}

instance Show ContinuedFraction where
  showsPrec p frac = showParen (p > 10) $ showString str
    where
      str = '[' : show (whole frac) ++ "; (" ++ intercalate ", " (map show $ fractions frac) ++ ")]"

sqrtParts :: Int -> ContinuedFraction
sqrtParts n
  | n == whole * whole = ContinuedFraction whole []
  | otherwise = ContinuedFraction whole $ inter 1 whole
  where
    whole = isqrt n
    inter :: Int -> Int -> [Int]
    inter num denom =
      let ugly = (n - denom * denom) `div` num
          result = (whole + denom) `div` ugly
          nextDenom = denom - result * ugly
          return = result : next ugly (abs nextDenom)
          text = unwords $ map show [num, denom, result, ugly, nextDenom]
       in -- if ugly == 0 then trace (unwords $ map show [n, num, denom]) return else return
          return
    next :: Int -> Int -> [Int]
    next 1 _ = []
    next num denom = inter num denom

main :: IO ()
main =
  let repeatingSections = map (fractions . sqrtParts) [2 .. 10000]
      oddCount = sum $ map ((`mod` 2) . length) repeatingSections
   in do
        -- print $ sqrtParts 23
        print oddCount
