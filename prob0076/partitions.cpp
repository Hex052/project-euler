#include <iostream>
#include <map>

/**
 * @brief The partition function P
 *
 * @param n Number to partition
 * @return int64_t The number of distinct partitions able to be created
 *
 * https://mathworld.wolfram.com/PartitionFunctionP.html
 * Euler's method to generate the partition number p
 */
int64_t partition(const int64_t n) noexcept {
	static std::map<int64_t, int64_t> memo{
	  {0, 1},     {1, 1},     {2, 2},     {3, 3},     {4, 5},     {5, 7},
	  {6, 11},    {7, 15},    {8, 22},    {9, 30},    {10, 42},   {11, 56},
	  {12, 77},   {13, 101},  {14, 135},  {15, 176},  {16, 231},  {17, 297},
	  {18, 385},  {19, 490},  {20, 627},  {21, 792},  {22, 1002}, {23, 1255},
	  {24, 1575}, {25, 1958}, {26, 2436}, {27, 3010}, {28, 3718}, {29, 4565},
	  {30, 5604},
	};
	try {
		return memo.at(n);
	}
	catch (const std::out_of_range &e) {
	}

	int64_t sum = 0;
	for (int64_t k = 1; k <= n; k++) {
		int64_t part =
		  partition(n - k * (3 * k - 1) / 2) + partition(n - k * (3 * k + 1) / 2);
		if (k % 2 == 1) {
			sum += part;
		}
		else {
			sum -= part;
		}
	}
	memo[n] = sum;
	return sum;
}

int main(int argc, const char *argv[]) {
	std::cout << partition(100) - 1 << std::endl;

	return 0;
}
