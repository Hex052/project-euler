#include "cardgroup.hpp"
#include <algorithm>


poker::cardgroup::cardgroup() : map_by_rank(), map_by_suit(), count(0) {
}
poker::cardgroup::cardgroup(const poker::card c1, const poker::card c2,
                            const poker::card c3, const poker::card c4,
                            const poker::card c5)
    : map_by_rank(), map_by_suit(), count(0) {
	this->add(c1);
	this->add(c2);
	this->add(c3);
	this->add(c4);
	this->add(c5);
}
poker::cardgroup::cardgroup(const poker::card cards[5])
    : map_by_rank(), map_by_suit(), count(0) {
	for (uint_fast8_t i = 0; i < 5; ++i) {
		this->add(cards[i]);
	}
}
poker::cardgroup::~cardgroup() {
}

bool poker::cardgroup::operator==(const poker::cardgroup &rhs) const {
	return this->map_by_rank == rhs.map_by_rank
	       && this->map_by_suit == rhs.map_by_suit;
}
bool poker::cardgroup::operator!=(const poker::cardgroup &rhs) const {
	return this->map_by_rank != rhs.map_by_rank
	       || this->map_by_suit != rhs.map_by_suit;
}

bool poker::cardgroup::add(const card c) {
	if (this->count == 5) {
		return false;
	}
	this->map_by_rank[c.rank].insert(c.suit);
	bool result = this->map_by_suit[c.suit].insert(c.rank).second;
	if (result) {
		++this->count;
	}
	return result;
}
bool poker::cardgroup::remove(const card c) {
	{
		poker::cardgroup::rank_map::iterator by_rank =
		  this->map_by_rank.find(c.rank);
		if (by_rank != this->map_by_rank.end()) {
			if (by_rank->second.size() == 1) {
				this->map_by_rank.erase(by_rank);
			}
			else {
				by_rank->second.erase(c.suit);
			}
		}
	}
	{
		poker::cardgroup::suit_map::iterator by_suit =
		  this->map_by_suit.find(c.suit);
		if (by_suit == this->map_by_suit.end()) {
			return false;
		}
		else if (by_suit->second.size() == 1) {
			this->map_by_suit.erase(by_suit);
			--this->count;
			return true;
		}
		bool result = by_suit->second.erase(c.rank) == 1;
		if (result) {
			--this->count;
		}
		return result;
	}
}
void poker::cardgroup::clear() {
	this->map_by_rank.clear();
	this->map_by_suit.clear();
	this->count = 0;
}

size_t poker::cardgroup::size() const noexcept {
	return this->count;
}

const poker::cardgroup::rank_map &poker::cardgroup::by_rank() const {
	return this->map_by_rank;
}
const poker::cardgroup::suit_map &poker::cardgroup::by_suit() const {
	return this->map_by_suit;
}

static bool
is_size_2(const poker::cardgroup::rank_map::const_iterator::value_type &i) {
	return i.second.size() == 2;
}
static bool
is_size_3(const poker::cardgroup::rank_map::const_iterator::value_type &i) {
	return i.second.size() == 3;
}

poker::Rank poker::cardgroup::high_card() const noexcept {
	return (--this->map_by_rank.cend())->first;
}
uint8_t poker::cardgroup::is_one_pair() const noexcept {
	if (this->map_by_rank.size() != 4) {
		return 0;
	}
	return (uint8_t)((std::find_if(this->map_by_rank.cbegin(),
	                               this->map_by_rank.cend(), is_size_2))
	                   ->first);
}
std::pair<uint8_t, uint8_t> poker::cardgroup::is_two_pair() const noexcept {
	if (this->map_by_rank.size() != 3) {
		return std::pair<uint8_t, uint8_t>(0, 0);
	}
	const poker::cardgroup::rank_map::const_iterator end =
	  this->map_by_rank.cend();
	poker::cardgroup::rank_map::const_iterator iter =
	  std::find_if(this->map_by_rank.cbegin(), end, is_size_2);
	return iter == end ? std::pair<uint8_t, uint8_t>(0, 0)
	                   : std::pair<uint8_t, uint8_t>(
	                     (uint8_t)iter->first,
	                     (uint8_t)std::find_if(iter, end, is_size_2)->first);
}
uint8_t poker::cardgroup::is_three_of_a_kind() const noexcept {
	if (this->map_by_rank.size() != 3) {
		return 0;
	}
	const poker::cardgroup::rank_map::const_iterator end =
	  this->map_by_rank.cend();
	poker::cardgroup::rank_map::const_iterator iter =
	  std::find_if(this->map_by_rank.cbegin(), end, is_size_3);
	return iter == end ? 0 : (uint8_t)iter->first;
}
uint8_t poker::cardgroup::is_straight() const noexcept {
	if (this->map_by_rank.size() != 5) {
		return 0;
	}
	uint8_t high_card = (uint8_t)(--this->map_by_rank.cend())->first;
	return (high_card - (uint8_t)(this->map_by_rank.cbegin()->first) == 4)
	         ? high_card
	         : 0;
}
std::tuple<uint8_t, uint8_t, uint8_t, uint8_t, uint8_t>
poker::cardgroup::is_flush() const noexcept {
	if (this->map_by_suit.size() != 1 || this->count != 5) {
		return {0, 0, 0, 0, 0};
	}
	poker::cardgroup::const_iterator iter = this->begin();
	return {(uint8_t)iter->rank, (uint8_t)(++iter)->rank, (uint8_t)(++iter)->rank,
	        (uint8_t)(++iter)->rank, (uint8_t)(++iter)->rank};
}
std::pair<uint8_t, uint8_t> poker::cardgroup::is_full_house() const noexcept {
	if (this->map_by_rank.size() != 2) {
		return std::pair<uint8_t, uint8_t>(0, 0);
	}
	poker::cardgroup::rank_map::const_iterator iter = this->map_by_rank.cbegin();
	if (iter->second.size() == 2) {
		uint8_t two = (uint8_t)iter->first;
		return std::pair<uint8_t, uint8_t>((uint8_t)(++iter)->first, two);
	}
	else if (iter->second.size() == 3) {
		return std::pair<uint8_t, uint8_t>((uint8_t)iter->first,
		                                   (uint8_t)(++iter)->first);
	}
	else {
		return std::pair<uint8_t, uint8_t>(0, 0);
	}
}
uint8_t poker::cardgroup::is_four_of_a_kind() const noexcept {
	if (this->map_by_rank.size() != 2) {
		return 0;
	}
	poker::cardgroup::rank_map::const_iterator iter = this->map_by_rank.cbegin();
	if (iter->second.size() == 1) {
		return (uint8_t)(++iter)->first;
	}
	else if (iter->second.size() == 4) {
		return (uint8_t)iter->first;
	}
	else {
		return 0;
	}
}
uint8_t poker::cardgroup::is_straight_flush() const noexcept {
	return (this->map_by_suit.size() == 0) ? this->is_straight() : 0;
}
// bool poker::cardgroup::is_royal_flush() const noexcept {
// 	return this->is_straight_flush() == (uint8_t)poker::Rank::Ace;
// }

poker::cardgroup::const_iterator poker::cardgroup::begin() const {
	return poker::cardgroup::const_iterator(false, *this);
}
poker::cardgroup::const_iterator poker::cardgroup::end() const {
	return poker::cardgroup::const_iterator(true, *this);
}
poker::cardgroup::const_reverse_iterator poker::cardgroup::rbegin() const {
	return std::make_reverse_iterator(this->end());
}
poker::cardgroup::const_reverse_iterator poker::cardgroup::rend() const {
	return std::make_reverse_iterator(this->begin());
}

std::strong_ordering poker::operator<=>(const cardgroup &lhs,
                                        const cardgroup &rhs) {
	std::strong_ordering order = lhs.size() <=> rhs.size();
	if (order != 0) {
		return order;
	}
	order = lhs.is_straight_flush() <=> rhs.is_straight_flush();
	if (order != 0) {
		return order;
	}
	order = lhs.is_four_of_a_kind() <=> rhs.is_four_of_a_kind();
	if (order != 0) {
		return order;
	}
	order = lhs.is_full_house() <=> rhs.is_full_house();
	if (order != 0) {
		return order;
	}
	order = lhs.is_flush() <=> rhs.is_flush();
	if (order != 0) {
		return order;
	}
	order = lhs.is_straight() <=> rhs.is_straight();
	if (order != 0) {
		return order;
	}
	order = lhs.is_three_of_a_kind() <=> rhs.is_three_of_a_kind();
	if (order != 0) {
		return order;
	}
	order = lhs.is_two_pair() <=> rhs.is_two_pair();
	if (order != 0) {
		return order;
	}
	order = lhs.is_one_pair() <=> rhs.is_one_pair();
	if (order != 0) {
		return order;
	}
	return lhs.high_card() <=> rhs.high_card();
}

//////////////////////// iter

inline static void update_card_referred_to(std::optional<poker::card> &card,
                                           poker::Rank rank, poker::Suit suit) {
	if (card) {
		card->rank = rank;
		card->suit = suit;
	}
	else {
		card.emplace(rank, suit);
	}
}

poker::cardgroup_iterator::cardgroup_iterator(bool is_end,
                                              const poker::cardgroup &container)
    : container(&container), map_iter(is_end ? container.map_by_rank.cend()
                                             : container.map_by_rank.cbegin()),
      set_iter(), card_referred_to() {
	if (!is_end) {
		this->set_iter.emplace(container.map_by_rank.cbegin()->second.begin());
		this->card_referred_to.emplace(this->map_iter->first, **this->set_iter);
	}
}
poker::cardgroup_iterator::~cardgroup_iterator() = default;

bool poker::cardgroup_iterator::operator==(
  const poker::cardgroup_iterator &rhs) const noexcept {
	return this->map_iter == rhs.map_iter && this->set_iter == rhs.set_iter;
}
bool poker::cardgroup_iterator::operator!=(
  const poker::cardgroup_iterator &rhs) const noexcept {
	return this->map_iter != rhs.map_iter || this->set_iter != rhs.set_iter;
}

const poker::card &poker::cardgroup_iterator::operator*() const noexcept {
	return *this->card_referred_to;
}
const poker::card *poker::cardgroup_iterator::operator->() const noexcept {
	return &*this->card_referred_to;
}

poker::cardgroup_iterator &poker::cardgroup_iterator::operator++() {
	// Undefined behavior results by incrementing past the end,
	// so it's fine to assume set_iter is valid and incrementing map_iter is fine.
	++*this->set_iter;
	if (this->set_iter == this->map_iter->second.end()) {
		++this->map_iter;
		if (this->map_iter == this->container->map_by_rank.end()) {
			this->set_iter.reset();
			this->card_referred_to.reset();
			return *this;
		}
		*this->set_iter = this->map_iter->second.begin();
	}
	update_card_referred_to(this->card_referred_to, this->map_iter->first,
	                        **this->set_iter);
	return *this;
}
poker::cardgroup_iterator poker::cardgroup_iterator::operator++(int) {
	poker::cardgroup_iterator temp(*this);
	++*this;
	return temp;
}
poker::cardgroup_iterator &poker::cardgroup_iterator::operator--() {
	// Might be the end, so set_iter might be false. Test for that.
	// Undefined behavior results by incrementing before the beginning,
	// so it's fine to assume it works on map_iter
	// and implement undefined behavior in that case.
	if (!this->set_iter || *this->set_iter == this->map_iter->second.cbegin()) {
		--this->map_iter;
		this->set_iter = this->map_iter->second.cend();
	}
	update_card_referred_to(this->card_referred_to, this->map_iter->first,
	                        *(--*this->set_iter));
	return *this;
}
poker::cardgroup_iterator poker::cardgroup_iterator::operator--(int) {
	poker::cardgroup_iterator temp(*this);
	--*this;
	return temp;
}
