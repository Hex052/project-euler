#include "card.hpp"
#include <stdexcept>
#include <stdint.h>


constexpr poker::Rank poker::rank_from_char(const char c) {
	if (c >= '2' && c <= '9') {
		return poker::Rank(c - '0');
	}
	else if (c == 'T') {
		return poker::Rank::Ten;
	}
	else if (c == 'J') {
		return poker::Rank::Jack;
	}
	else if (c == 'Q') {
		return poker::Rank::Queen;
	}
	else if (c == 'K') {
		return poker::Rank::King;
	}
	else if (c == 'A') {
		return poker::Rank::Ace;
	}
	else {
		throw std::out_of_range("character must be one of '23456789TJQKA'");
	}
}
constexpr poker::Suit poker::suit_from_char(const char c) {
	switch (c) {
		case 'H':
			return poker::Suit::Hearts;
		case 'D':
			return poker::Suit::Diamonds;
		case 'C':
			return poker::Suit::Clubs;
		case 'S':
			return poker::Suit::Spades;
		default:
			throw std::out_of_range("character must be one of 'HDCS'");
	}
}

poker::card::card(poker::Rank r, poker::Suit s) : rank(r), suit(s) {
}
poker::card::card(const char *str)
    : rank(poker::rank_from_char(str[0])), suit(poker::suit_from_char(str[1])) {
}
poker::card::card(char r, char s)
    : rank(poker::rank_from_char(r)), suit(poker::suit_from_char(s)) {
}
