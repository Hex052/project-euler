#ifndef _CARD_HPP
#define _CARD_HPP

#include <compare>


namespace poker {

enum class Suit {
	Diamonds,
	Hearts,
	Clubs,
	Spades,
};
enum class Rank {
	Ace = 14,
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
};

/**
 * @brief Converts a character to a Rank enum
 *
 * @param c
 * @return Rank
 *
 * @exception std::out_of_range If the character is not in '23456789JQKA'
 */
constexpr Rank rank_from_char(const char c);
/**
 * @brief Converts a character to a Suit enum
 *
 * @param c
 * @return Suit
 *
 * @exception std::out_of_range If the character is not in 'HDCS'
 */
constexpr Suit suit_from_char(const char c);


class card {
public:
	Rank rank;
	Suit suit;

	card(Rank r, Suit s);
	card(char r, char s);
	card(const char *);
};

} // namespace poker

#endif
