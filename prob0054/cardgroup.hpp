#ifndef _CARDGROUP_HPP
#define _CARDGROUP_HPP

#include "card.hpp"
#include <map>
#include <optional>
#include <set>
#include <stdint.h>


namespace poker {

class cardgroup_iterator;

class cardgroup {
	friend class cardgroup_iterator;

private:
public:
	typedef std::map<Suit, std::set<Rank>> suit_map;
	typedef std::map<Rank, std::set<Suit>> rank_map;

	typedef cardgroup_iterator const_iterator;
	typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

	cardgroup();
	cardgroup(const card, const card, const card, const card, const card);
	cardgroup(const card[5]);
	virtual ~cardgroup();

	bool operator==(const cardgroup &rhs) const;
	bool operator!=(const cardgroup &rhs) const;

	bool add(const card);
	bool remove(const card);
	void clear();

	size_t size() const noexcept;

	const rank_map &by_rank() const;
	const suit_map &by_suit() const;

	Rank high_card() const noexcept;
	uint8_t is_one_pair() const noexcept;
	std::pair<uint8_t, uint8_t> is_two_pair() const noexcept;
	uint8_t is_three_of_a_kind() const noexcept;
	uint8_t is_straight() const noexcept;
	std::tuple<uint8_t, uint8_t, uint8_t, uint8_t, uint8_t>
	is_flush() const noexcept;
	std::pair<uint8_t, uint8_t> is_full_house() const noexcept;
	uint8_t is_four_of_a_kind() const noexcept;
	uint8_t is_straight_flush() const noexcept;
	// bool is_royal_flush() const noexcept;


	const_iterator begin() const;
	const_iterator end() const;
	const_reverse_iterator rbegin() const;
	const_reverse_iterator rend() const;

protected:
	rank_map map_by_rank;
	suit_map map_by_suit;
	uint8_t count;
};

std::strong_ordering operator<=>(const cardgroup &lhs, const cardgroup &rhs);

/**
 * @brief Iterates over the cards in order of rank. First 2s, then 3s, etc.
 */
class cardgroup_iterator {
	friend class cardgroup;

private:
	const cardgroup *container;
	cardgroup::rank_map::const_iterator map_iter;
	std::optional<cardgroup::rank_map::node_type::mapped_type::const_iterator>
	  set_iter;
	std::optional<card> card_referred_to;

protected:
	/**
	 * @brief Construct a new cardgroup iterator object
	 *
	 * @param is_end True if this container should start at the end,
	 *  otherwise will start at the beginning.
	 * @param container The container to iterate over
	 */
	cardgroup_iterator(bool is_start, const cardgroup &container);

public:
	using iterator_category = std::bidirectional_iterator_tag;
	using value_type = const card;
	using difference_type = std::ptrdiff_t;
	using pointer = const card *;
	using reference = const card &;

	cardgroup_iterator() = default;
	cardgroup_iterator(const cardgroup_iterator &) = default;
	cardgroup_iterator(cardgroup_iterator &&) = default;
	virtual ~cardgroup_iterator();

	cardgroup_iterator &operator=(const cardgroup_iterator &) = default;
	cardgroup_iterator &operator=(cardgroup_iterator &&) = default;

	const card &operator*() const noexcept;
	const card *operator->() const noexcept;

	bool operator==(const cardgroup_iterator &rhs) const noexcept;
	bool operator!=(const cardgroup_iterator &rhs) const noexcept;

	cardgroup_iterator &operator++();
	cardgroup_iterator operator++(int);
	cardgroup_iterator &operator--();
	cardgroup_iterator operator--(int);
};

} // namespace poker

#endif
