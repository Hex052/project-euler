#ifndef _HAND_HPP
#define _HAND_HPP

#include "card.hpp"
#include "cardgroup.hpp"
#include <string>


namespace poker {

class hand {
private:
	card cards[5];
	cardgroup group_cards() const;

public:
	hand(card, card, card, card, card);
	hand(card c[5]);
	virtual ~hand();


	constexpr card &operator[](const unsigned short pos);
	constexpr const card &operator[](const unsigned short pos) const;
	card &at(const unsigned short pos);
	const card &at(const unsigned short pos) const;

	std::weak_ordering operator<=>(const hand &rhs) const;
};

} // namespace poker

#endif
