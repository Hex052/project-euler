#include "card.hpp"
#include "cardgroup.hpp"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <regex>
#include <string>

// std::ostream operator<<(std::ostream &lhs, const poker::card &rhs) {
// 	switch (rhs.rank) {
// 		case /* constant-expression */:
// 			/* code */
// 			break;

// 		default:
// 			break;
// 	}
// }

int main(int argc, const char *argv[]) {
	if (argc != 2) {
		std::cerr << "usage: " << argv[0] << " [poker_file]" << std::endl;
		return 1;
	}
	std::ifstream pokerfile(argv[1]);
	if (!pokerfile.good()) {
		std::cerr << "Cannot open file" << std::endl;
		return 1;
	}
	std::string line;
	std::regex matchline("^"
	                     "([23456789TJQKA][HDCS]) ?"
	                     "([23456789TJQKA][HDCS]) ?"
	                     "([23456789TJQKA][HDCS]) ?"
	                     "([23456789TJQKA][HDCS]) ?"
	                     "([23456789TJQKA][HDCS]) ?"
	                     "([23456789TJQKA][HDCS]) ?"
	                     "([23456789TJQKA][HDCS]) ?"
	                     "([23456789TJQKA][HDCS]) ?"
	                     "([23456789TJQKA][HDCS]) ?"
	                     "([23456789TJQKA][HDCS])\\n?"
	                     "$");
	uint32_t p1wins = 0, p2wins = 0, draw = 0;
	size_t linenum = 0;
	while (std::getline(pokerfile, line)) {
		++linenum;
		if (line.empty() || std::all_of(line.cbegin(), line.cend(), isspace)) {
			continue;
		}

		std::match_results<std::string::const_iterator> line_results;
		if (!std::regex_match(line, line_results, matchline)) {
			std::cerr << "parse error. " << argv[1] << ':' << linenum
			          << "\nlines must not be blank and match "
			             "/^[23456789TJQKA][HDCS](?: [23456789TJQKA][HDCS]){9}$/"
			          << std::endl;
			return 1;
		}

		poker::cardgroup p1(poker::card(line_results[1].str().c_str()),
		                    poker::card(line_results[2].str().c_str()),
		                    poker::card(line_results[3].str().c_str()),
		                    poker::card(line_results[4].str().c_str()),
		                    poker::card(line_results[5].str().c_str()));
		poker::cardgroup p2(poker::card(line_results[6].str().c_str()),
		                    poker::card(line_results[7].str().c_str()),
		                    poker::card(line_results[8].str().c_str()),
		                    poker::card(line_results[9].str().c_str()),
		                    poker::card(line_results[10].str().c_str()));

		std::strong_ordering order = p1 <=> p2;
		if (order == std::strong_ordering::less) {
			p2wins += 1;
		}
		else if (order == std::strong_ordering::greater) {
			p1wins += 1;
		}
		else /* order == std::strong_ordering::equivalent */ {
			draw += 1;
			poker::cardgroup::const_iterator iter = p1.begin(), end = p1.end();
			std::cout << "draw: " << line << std::endl;
		}
	}
	std::cout << "Player 1: " << p1wins << "\n"
	          << "Player 2: " << p2wins << "\n"
	          << "Draw:     " << draw << std::endl;
	return 0;
}
