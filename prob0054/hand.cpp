#include "hand.hpp"
#include <algorithm>
#include <stdexcept>


poker::cardgroup poker::hand::group_cards() const {
	poker::cardgroup group;
	for (uint16_t i = 0; i < 5; i++) {
		group.add(this->cards[i]);
	}
	return group;
}
poker::hand::hand(poker::card c1, poker::card c2, poker::card c3,
                  poker::card c4, poker::card c5)
    : cards{c1, c2, c3, c4, c5} {
}
poker::hand::hand(poker::card c[5]) : cards{c[0], c[1], c[2], c[3], c[4]} {
}
poker::hand::~hand() {
}

constexpr poker::card &poker::hand::operator[](const unsigned short pos) {
	return this->cards[pos];
}
constexpr const poker::card &
poker::hand::operator[](const unsigned short pos) const {
	return this->cards[pos];
}
poker::card &poker::hand::at(const unsigned short pos) {
	if (pos >= 5) {
		throw std::out_of_range("hand only made of 5 cards");
	}
	return this->cards[pos];
}
const poker::card &poker::hand::at(const unsigned short pos) const {
	if (pos >= 5) {
		throw std::out_of_range("hand only made of 5 cards");
	}
	return this->cards[pos];
}

std::weak_ordering poker::hand::operator<=>(const hand &rhs) const {
	const poker::cardgroup lhs_group = this->group_cards();
	const poker::cardgroup rhs_group = rhs.group_cards();

	bool lhs_flush = lhs_group.is_flush();
	bool rhs_flush = rhs_group.is_flush();
	bool lhs_straight = lhs_group.is_straight();
	bool rhs_straight = rhs_group.is_straight();

	// Check for straight flush + royal flush
	if (lhs_flush && lhs_straight) {
		if (!(rhs_flush && rhs_straight)) {
			return std::weak_ordering::greater;
		}
		// Check for a royal flush (really, just high-card comparing)
		// Get the last-ranked card and compare them
		auto order = rhs_group.by_rank().rbegin()->first
		             <=> lhs_group.by_rank().rbegin()->first;
		if (order != 0) {
			// TODO is this needed?
			return order;
		}
	}
	else if (rhs_flush && rhs_straight) {
		return std::weak_ordering::less;
	}

	// Check for four of a kind and full house
	// TODO If the two hands have the same ranks, do we check for a straight?
	if (lhs_group.by_rank().size() == 2) {
		if (rhs_group.by_rank().size() != 2) {
			// rhs is neither a full house nor four of a kind
			return std::weak_ordering::greater;
		}
		// both could be
		auto lhs_size = lhs_group.by_rank().begin()->second.size(); ///Size of first
		auto rhs_size = rhs_group.by_rank().begin()->second.size(); ///Size of first
		if (lhs_size == 1 || lhs_size == 4) {
			if (rhs_size != 1 && rhs_size != 4) {
				return std::weak_ordering::greater;
			}
			// both are four of a kind, do high card comparison
			auto order = ((lhs_size == 4) ? lhs_group.by_rank().begin()->first
			                              : lhs_group.by_rank().rbegin()->first)
			             <=> ((rhs_size == 4) ? rhs_group.by_rank().begin()->first
			                                  : rhs_group.by_rank().rbegin()->first);
			if (order != 0) {
				return order;
			}
			// Both have the same with that, so compare set of one
			order = ((lhs_size == 1) ? lhs_group.by_rank().begin()->first
			                         : lhs_group.by_rank().rbegin()->first)
			        <=> ((rhs_size == 1) ? rhs_group.by_rank().begin()->first
			                             : rhs_group.by_rank().rbegin()->first);
			if (order != 0) {
				// TODO is this needed?
				return order;
			}
		}
		else if (rhs_size == 1 || rhs_size == 4) {
			return std::weak_ordering::less;
		}

		// both are a full house, do high card comparison
		auto order = ((lhs_size == 3) ? lhs_group.by_rank().begin()->first
		                              : lhs_group.by_rank().rbegin()->first)
		             <=> ((rhs_size == 3) ? rhs_group.by_rank().begin()->first
		                                  : rhs_group.by_rank().rbegin()->first);
		if (order != 0) {
			return order;
		}
		// Both have the same with that, so compare set of one
		// order = ((lhs_size == 2) ? lhs_group.by_rank().begin()->first
		//                          : lhs_group.by_rank().rbegin()->first)
		//         <=> ((rhs_size == 2) ? rhs_group.by_rank().begin()->first
		//                              : rhs_group.by_rank().rbegin()->first);
		// if (order != 0) {
		// 	return order;
		// }
	}
	else if (rhs_group.by_rank().size() == 2) {
		return std::weak_ordering::less;
	}

	// Check for a flush
	if (lhs_flush) {
		if (rhs_flush) {
			// Both are flushes, so compare high rank
			auto order = rhs_group.by_rank().rbegin()->first
			             <=> lhs_group.by_rank().rbegin()->first;
			if (order != 0) {
				// TODO is this needed?
				return order;
			}
		}
		return std::weak_ordering::greater;
	}
	else if (rhs_flush) {
		return std::weak_ordering::less;
	}
	// Check for a straight
	if (lhs_straight) {
		if (rhs_straight) {
			// Both are straights, so compare high rank
			auto order = rhs_group.by_rank().rbegin()->first
			             <=> lhs_group.by_rank().rbegin()->first;
			if (order != 0) {
				// TODO is this needed?
				return order;
			}
		}
		return std::weak_ordering::greater;
	}
	else if (rhs_straight) {
		return std::weak_ordering::less;
	}

	// Check for two pair and three of a kind
	if (lhs_group.by_rank().size() == 3) {
		if (rhs_group.by_rank().size() != 3) {
			// rhs is neither two pair nor three of a kind
			return std::weak_ordering::greater;
		}
		// both could be
		auto lhs_iter =
		  lhs_group.by_rank().rbegin();          /// first one where isn't size 1
		auto lhs_size = lhs_iter->second.size(); /// Size, either 2 or 3
		while (lhs_size == 1) {                  // May take 2 steps: (1, 1, 3)
			++lhs_iter;
			lhs_size = lhs_iter->second.size();
		}
		auto rhs_iter =
		  rhs_group.by_rank().rbegin();          /// first one where isn't size 1
		auto rhs_size = rhs_iter->second.size(); /// Size, either 2 or 3
		while (rhs_size == 1) {                  // May take 2 steps: (1, 1, 3)
			++rhs_iter;
			rhs_size = rhs_iter->second.size();
		}

		if (lhs_size == 3) {
			if (rhs_size != 3) {
				return std::weak_ordering::greater;
			}
			// both are 3 of a kind, do high card comparison
			auto order = lhs_iter->first <=> rhs_iter->first;
			if (order != 0) {
				return order;
			}
		}
		else if (rhs_size == 3) {
			return std::weak_ordering::less;
		}

		// both are a two of a kind, do high card comparison
		auto order = lhs_iter->first <=> rhs_iter->first;
		if (order != 0) {
			return order;
		}
		// Both have the same with first, so compare next
		order = (++lhs_iter)->first <=> (++rhs_iter)->first;
		if (order != 0) {
			return order;
		}
	}
	else if (rhs_group.by_rank().size() == 3) {
		return std::weak_ordering::less;
	}

	// TODO one pair
	// Check for one pair
	if (lhs_group.by_rank().size() == 4) {
		if (rhs_group.by_rank().size() != 4) {
			return std::weak_ordering::greater;
		}
		// Both are one pair, so compare rank of that one pair
		auto lhs_iter =
		  lhs_group.by_rank().rbegin();        /// first one where isn't size 1
		while (lhs_iter->second.size() == 1) { // May take 3 steps: (1, 1, 1, 3)
			++lhs_iter;
		}
		auto rhs_iter =
		  rhs_group.by_rank().rbegin();        /// first one where isn't size 1
		while (rhs_iter->second.size() == 1) { // May take 3 steps: (1, 1, 1, 3)
			++rhs_iter;
		}
		auto order = lhs_iter->first <=> rhs_iter->first;
		if (order != 0) {
			return order;
		}
	}
	else if (rhs_group.by_rank().size() == 4) {
		return std::weak_ordering::less;
	}

	// These hands are terrible. Compare high cards.
	std::strong_ordering order = std::strong_ordering::equal;
	for (poker::cardgroup::const_reverse_iterator lhs_iter = lhs_group.rbegin(),
	                                              rhs_iter = rhs_group.rbegin();
	     lhs_iter != lhs_group.rend() && rhs_iter != rhs_group.rend();
	     ++lhs_iter, ++rhs_iter) {
		order = lhs_iter->rank <=> rhs_iter->rank;
		if (order != 0) {
			return order;
		}
	}
	return order;
}
