#include <stdio.h>

unsigned long squareandadd(unsigned long num);

int main(int argc, char const *argv[]) {
	int at89 = 0;
	char str[128], *ptr;
	for (unsigned long i = 1; i < 10000000; i++) {
		unsigned long num = i;
		while (num != 89 && num != 1) {
			snprintf(str, 128, "%lu", num);
			num = 0;
			ptr = str;
			do {
				*ptr -= '0';
				num += *ptr * *ptr;
				ptr++;
			} while (*ptr != '\0');
		}
		if (num == 89) {
			at89++;
		}
	}
	printf("%d\n", at89);
	return 0;
}
