#include <gmp.h>
#include <math.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

int main(int argc, char const *argv[]) {
	uint64_t a = 3;
	uint64_t b = a - 1;
	uint64_t b_squared = b * b;
	uint64_t a_squared = a * a;
	uint64_t perim = 2 * a + b;

	uint64_t total_perim = 0;
	mpz_t area, square_area, area_err;
	mpz_inits(area, square_area, area_err, NULL);

	for (; b <= 1000000000 / 3;
	     b_squared = a_squared, ++b, ++a, a_squared = a * a, perim += 3) {
#ifdef DEBUG
		if (a == 333333332) {
			b = a - 1;
		}
#endif

		// T = (b / 4) * sqrt(4 * a**2 - b**2)
		// Where sides are (a, a, b)
		mpz_set_ui(square_area, (4 * a_squared - b_squared));
		mpz_mul_ui(square_area, square_area, b_squared); // square_area *= b_squared
		mpz_sqrtrem(area, area_err, square_area);
		if (mpz_sgn(area_err) == 0 && mpz_mod_ui(area_err, area, 4) == 0) {
			total_perim += perim;
		}

		// // area = sqrt((4 * a_squared - b_squared) * b_squared / 16);
		// if (area == (int)area) {
		// }

		// Try the opposite
		// area = sqrt(4 * b_squared - a_squared) * a / 4;
		// if (area == (int)area) {
		// 	total_perim += perim - 1;
		// }
		mpz_set_ui(square_area, (4 * b_squared - a_squared));
		mpz_mul_ui(square_area, square_area, a_squared); // square_area *= a_squared
		mpz_sqrtrem(area, area_err, square_area);
		if (mpz_sgn(area_err) == 0 && mpz_mod_ui(area_err, area, 4) == 0) {
			total_perim += perim - 1;
		}
	}
	// consider case where a is 333333333 and b is 333333334
	// a = 333333333;
	// b = a + 1;
	// b_squared = b * b;
	// a_squared = a * a;
	// perim = 2 * a + b;
	// area = sqrt(4 * a_squared - b_squared) * b / 4;
	// if (area == (int)area) {
	// 	total_perim += perim;
	// }

	printf("Total perimiter: %ld\n", total_perim);

	return 0;
}
