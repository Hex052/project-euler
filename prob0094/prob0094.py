import math
total = 0

for b in range(2,1000000000//3+3,2):
    h_pos_squared = (3*b//2+1)*(b//2+1)
    h_pos_root = math.isqrt(h_pos_squared)
    if h_pos_squared == h_pos_root*h_pos_root and 3*b+2 <= 1000000000:
        total += 3*b + 2
    h_neg_squared = (3*b//2-1)*(b//2-1)
    h_neg_root = math.isqrt(h_neg_squared)
    if h_neg_squared == h_neg_root*h_pos_root and 3*b-2 <= 1000000000:
        total += 3*b - 2

print(total)