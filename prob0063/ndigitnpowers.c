#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <gmp.h>

int main(int argc, char const *argv[]) {
	int nums = 0;
	char str[128];
	mpz_t num;
	mpz_init(num);
	for (unsigned long i = 1; i < 10; i++) {
		mpz_set_ui(num, i);
		for (unsigned long j = 1;; j++) {
			//j is the current power, as num is multiplied by i every loop
			//If length of decimal number is the power
			if (j == strlen(mpz_get_str(str, 10, num))) {
				printf("%lu^%lu\t%s\n", i, j, str);
				nums++;
				mpz_mul_ui(num, num, i);
			}
			else {
				break;
			}
		}
	}
	printf("%d\n", nums);
	return 0;
}
