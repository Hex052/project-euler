#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <set>
#include <stdint.h>
#include <string>

typedef std::string set_of;
typedef std::pair<set_of, set_of> pair_t;

struct less_string_numerically {
	bool operator()(const set_of &lhs, const set_of &rhs) const noexcept {
		if (lhs.size() < rhs.size()) {
			return true;
		}
		else if (lhs.size() > rhs.size()) {
			return false;
		}
		else {
			return lhs < rhs;
		}
	}
	bool operator()(const pair_t &lhs, const pair_t &rhs) const noexcept {
		if (lhs.first.size() < rhs.first.size()) {
			return true;
		}
		else if (lhs.first.size() > rhs.first.size()) {
			return false;
		}
		else if (lhs.first < rhs.first) {
			return true;
		}
		if (lhs.second.size() < rhs.second.size()) {
			return true;
		}
		else if (lhs.second.size() > rhs.second.size()) {
			return false;
		}
		else {
			return lhs.second < rhs.second;
		}
	}
};

typedef std::set<set_of, less_string_numerically> set_t;
typedef std::map<set_of, set_t, less_string_numerically> pairs_t;

template class std::map<set_of, set_t, less_string_numerically>;
template class std::set<set_of, less_string_numerically>;
template class std::basic_string<char>;

inline std::string &cat(std::string &set, const std::string &begin,
                        const std::string &end) {
	set.assign(begin);
	set.append(end);
	return set;
}

int main(int argc, char const *argv[]) {
	if (argc != 2) {
		std::cerr << "Usage: " << argv[0] << " plist" << std::endl;
		return 1;
	}
	std::ifstream primefile(argv[1]);
	if (!primefile.good()) {
		std::cerr << "Cannot open file" << std::endl;
		return 1;
	}

	set_t all_primes;
	pair_t two_primes;
	two_primes.first.reserve(9);
	two_primes.second.reserve(9);
	set_of line;
	pairs_t one, both;
	line.reserve(10);

	while (std::getline(primefile, line)) {
		all_primes.insert(line);

		two_primes.first.clear();
		two_primes.second.clear();
		const size_t end = line.size() - 1;

		for (size_t i = 0; i != end;) {
			two_primes.first += line[i];
			++i;
			if (!all_primes.contains(two_primes.first)) {
				continue;
			}
			two_primes.second.assign(line.c_str() + i, line.size() - i);
			if (!all_primes.contains(two_primes.second)) {
				continue;
			}

			// Wooo, we found one! Save it!

			// Check if there isn't a reverse pair
			{
				pairs_t::iterator iter;
				iter = one.find(two_primes.second);
				if (iter == one.end() || !iter->second.contains(two_primes.first)) {
					one[two_primes.first].insert(two_primes.second);
					continue;
				}
				// We have both
				one.erase(iter);
			}
			both[two_primes.second].insert(two_primes.first);
			// The prime numbers that
			set_t &first = both[two_primes.first];
			first.insert(two_primes.second);

			// needs to contain four others. Won't contain itself, so four.
			if (first.size() < 4) {
				continue;
			}
			set_t &second = both[two_primes.second];
			if (second.size() < 4) {
				continue;
			}

			// Now check if we have a good match
			// We know two of the values that should be there now.
			// The intersection of set and iter->second should contain at least three
			// values.
			std::map<set_of, set_t &, less_string_numerically> intersection;
			{
				const less_string_numerically l;
				auto iter = first.begin(), end = first.end();
				auto iter_search = second.begin(), end_search = second.end();
				while (iter != end && iter_search != end_search) {
					if (*iter == *iter_search) {
						set_t &items = both[*iter];
						if (items.size() < 4) {
							intersection.emplace(*iter, items);
							// intersection[*iter] = items;
						}
						++iter;
						++iter_search;
					}
					else if (l(*iter, *iter_search)) {
						++iter;
					}
					else {
						++iter_search;
					}
				}
			}
			// Won't contain the first or second (they aren't part of the intersection).
			// At least three should be left.
			if (intersection.size() < 3) {
				continue;
			}

			std::cout << "possible: " << two_primes.first << ' ' << two_primes.second
			          << '\n';
			for (const std::pair<set_of, set_t &> &pair : intersection) {
				std::cout << '\t' << pair.first << " : ";
				for (const set_of &s : pair.second) std::cout << s << ' ';
				std::cout << '\n';
			}
			std::cout << std::endl;
		}
	}

	return 0;
}
