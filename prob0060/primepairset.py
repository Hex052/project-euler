#!/usr/bin/env python3
import itertools
import os

with open(os.path.join(os.path.dirname(__file__), "..", "nplist.txt"), "r") as f:
    all_prime_str = set(filter(bool, map(str.rstrip, f)))
print("loaded part")
all_prime_int = {int(i): i for i in all_prime_str}
print("loaded")


for one, one_str in all_prime_int.items():
    for two, two_str in all_prime_int.items():
        if two == one:
            break
        if (
            one_str + two_str not in all_prime_str
            or two_str + one_str not in all_prime_str
        ):
            continue
        for three, three_str in all_prime_int.items():
            if three == two:
                break
            if (
                one_str + three_str not in all_prime_str
                or three_str + one_str not in all_prime_str
                or two_str + three_str not in all_prime_str
                or three_str + two_str not in all_prime_str
            ):
                continue
            for four, four_str in all_prime_int.values():
                if four == three:
                    break
                if (
                    one_str + four_str not in all_prime_str
                    or four_str + one_str not in all_prime_str
                    or two_str + four_str not in all_prime_str
                    or four_str + two_str not in all_prime_str
                    or three_str + four_str not in all_prime_str
                    or four_str + three_str not in all_prime_str
                ):
                    continue
                for five, five_str in all_prime_int.values():
                    if five == four:
                        break
                    if (
                        one_str + five_str not in all_prime_str
                        or five_str + one_str not in all_prime_str
                        or two_str + five_str not in all_prime_str
                        or five_str + two_str not in all_prime_str
                        or three_str + five_str not in all_prime_str
                        or five_str + three_str not in all_prime_str
                        or four_str + five_str not in all_prime_str
                        or five_str + four_str not in all_prime_str
                    ):
                        continue
                    total = one + two + three + four + five
                    print(
                        f"sum: {total:>10} | {one_str:>10} {two_str:>10} "
                        f"{three_str:>10} {four_str:>10} {five_str:>10}"
                    )
