#ifndef HEADER_PERMUTESTR
#define HEADER_PERMUTESTR

#include <string.h>
#include <stdbool.h>

void swapchar(char *a, char *b) { //pointers to single characters, not to strings.
	char k = *a;
	*a = *b;
	*b = k;
	return;
}

//! Lexicographic permutations of str. Set len to 0 to permute the whole string.
//! Returns true if was permuted
bool permutestr(char *str, int len) {
	if (len == 0) {
		len = strlen(str);
	}

	int i = len - 1, j = len;

	while (str[i - 1] >= str[i]) {
		i--;
	}
	while (str[j - 1] <= str[i - 1]) {
		j--;
	}

	if (i == 0) {
		return false;
	}
	swapchar(&str[i - 1], &str[j - 1]);
	i++; j = len;

	while (i < j) {
		swapchar(&str[i - 1], &str[j - 1]);
		i++;
		j--;
	}
	return true;
}


#endif
