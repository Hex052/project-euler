#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <gmp.h>


void reverse(mpz_t rop, mpz_t op);
int ispal(mpz_t num);

int main(int argc, char const *argv[]) {
	mpz_t num, rev;
	mpz_inits(num, rev, NULL);

	int palindromes = 0;
	for (int i = 1; i < 10000; i++) {
		mpz_set_si(num, i);
		// if (ispal(num)) {
		// 	palindromes++;
		// 	continue;
		// }
		for (int j = 1; j < 50; j++) {
			// num += reverse(num);
			reverse(rev, num);
			mpz_add(num, num, rev);
			if (ispal(num)) {
				palindromes++;
				break;
			}
		}
	}

	printf("palindromes: %d\nLychrel nums: %d\n", palindromes, 9999 - palindromes);

	return 0;
}

void reverse(mpz_t rop, mpz_t op) {
	mpz_t copy;
	mpz_init_set(copy, op);
	mpz_set_si(rop, 0);
	while(mpz_cmp_si(copy, 0) != 0) {
		mpz_mul_si(rop, rop, 10);
		mpz_add_ui(rop, rop, mpz_fdiv_q_ui(copy, copy, 10));
	}
	mpz_clear(copy);
}
int ispal(mpz_t num) {
	char *str = mpz_get_str(NULL, 10, num), *fwd = str, *rev = ((char*)memchr(str, 0, 50)) - 1;
	while (rev > fwd) {
		if (*rev != *fwd) {
			free(str);
			return 0;
		}

		fwd++; rev--;
	}

	free(str);
	return 1;
}
