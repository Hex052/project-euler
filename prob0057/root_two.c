#include <gmp.h>
#include <stdbool.h>
#include <stdio.h>

int main(int argc, char const *argv[]) {
	bool verbose = false;

	char dest[512];
	int count = 0;
	mpq_t frac;
	mpq_init(frac);
	mpq_set_ui(frac, 1, 2);
	mpz_t numerator; //! For when a specific reference to the numerator is needed
	mpz_init(numerator);
	for (int i = 1; i < 1000; ++i) {
		// Increase the numerator
		// numerator += 2 * denominator;
		mpz_addmul_ui(mpq_numref(frac), mpq_denref(frac), 2);

		// Swap the numerator and denominator
		mpq_inv(frac, frac);

		// Check for where the numerator is longer than the denominator
		mpq_get_num(numerator, frac);
		mpz_add(numerator, numerator, mpq_denref(frac));
		size_t numerator_len = gmp_snprintf(dest, 512, "%Zd", numerator);
		size_t denominator_len = gmp_snprintf(dest, 512, "%Zd", mpq_denref(frac));
		if (numerator_len > denominator_len) {
			if (verbose) {
				gmp_fprintf(stdout, "Expansion: %d\n N: %Zd\n D: %Zd\n", i + 1,
				            numerator, mpq_denref(frac));
			}
			++count;
		}
	}
	mpz_clear(numerator);
	mpq_clear(frac);
	fprintf(stderr, "%d\n", count);
	return 0;
}
