clear
clc

A = zeros(100);
R = zeros(100);

for m = 1:100
    for n = 1:100
        R(m,n,1) = m*n;
        R(m,n,2) = 0.25*(m*m+m)*(n*n+n);
    end
end

R_adj = abs(R(:,:,2) - 2000000);
B=R(:,:,1);
B(R_adj==min(R_adj,[],'all'))