#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

uint_fast32_t next_prime_num(FILE *stream, char **line, size_t *n,
                             ssize_t *digits) {
	*digits = getdelim(line, n, '\n', stream) - 1;
	if (*digits < 0) {
		fprintf(stderr, "not enough prime numbers!\n");
		exit(2);
	}
	(*line)[*digits] = '\0';
	return strtoul(*line, NULL, 10);
}

int main(int argc, char const *argv[]) {
	const char *default_name = "./nplist.txt";
	FILE *nplist = NULL;
	if (argc == 2) {
		nplist = fopen(argv[1], "r");
	}
	else if (argc == 1) {
		nplist = fopen("./nplist.txt", "r");
	}
	else {
		fprintf(stderr,
		        "usage: %s [plist]\n"
		        "plist is a path to a file containing prime "
		        "numbers separated by newlines. Defaults to %s\n",
		        argv[0], default_name);
	}

	if (nplist == NULL) {
		int err = errno;
		fprintf(stderr, "Error %d opening %s : %s\n", err,
		        argc == 1 ? default_name : argv[1], strerror(err));
		exit(1);
	}


	uint_fast32_t diagonals = 1;
	uint_fast32_t primes = 0;
	uint_fast32_t size = 1;
	uint_fast32_t current_corner = 1;
	uint_fast32_t next_prime;
	char *line;
	size_t n = 0;
	ssize_t digits;

	digits = getdelim(&line, &n, '\n', nplist) - 1;
	if (digits < 0) {
		fprintf(stderr, "not enough prime numbers!\n");
		exit(2);
	}
	next_prime = next_prime_num(nplist, &line, &n, &digits);

	do {
		++size;
		for (int i = 0; i < 4; i++) {
			++diagonals;
			current_corner += size;
			while (current_corner > next_prime) {
				next_prime = next_prime_num(nplist, &line, &n, &digits);
			}
			if (current_corner == next_prime) {
				++primes;
				next_prime = next_prime_num(nplist, &line, &n, &digits);
			}
		}
		++size;
	} while ((float)primes / (float)diagonals > .1);
	printf("%lu\n", size);

	return 0;
}
