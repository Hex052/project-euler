"""
Project Euler Problem 89
https://projecteuler.net/problem=89
"""
import os
import argparse
from typing import Iterable, Union


class RomanNumeral:
    VALUES = {
        "I": 1,
        "V": 5,
        "X": 10,
        "L": 50,
        "C": 100,
        "D": 500,
        "M": 1000,
    }

    def __init__(self, num: Union[int, str]):
        if isinstance(num, int):
            self.num = num
            return
        if not isinstance(num, str):
            raise TypeError("Expected int or str, got %s" % type(num).__name__)

        # parse out the number
        self.num = 0
        last = None
        for i in map(lambda x: self.VALUES[x], num):
            if last is None:
                last = i
            elif i > last:
                self.num += i - last
                last = None
            else:
                self.num += last
                last = i
        if last is not None:
            self.num += last

    def __str__(self) -> str:
        out = []
        thousands = self.num // 1000
        out.append('M' * thousands)

        hundreds = self.num % 1000 // 100
        if hundreds == 4:
            out.append("CD")
        elif hundreds == 9:
            out.append("CM")
        else:
            if hundreds >= 5:
                out.append("D")
                hundreds -= 5
            out.append("C" * hundreds)

        tens = self.num % 100 // 10
        if tens == 4:
            out.append("XL")
        elif tens == 9:
            out.append("XC")
        else:
            if tens >= 5:
                out.append("L")
                tens -= 5
            out.append("X" * tens)

        ones = self.num % 10
        if ones == 4:
            out.append("IV")
        elif ones == 9:
            out.append("IX")
        else:
            if ones >= 5:
                out.append("V")
                ones -= 5
            out.append("I" * ones)

        return ''.join(out)

def numerals_saved(lines: Iterable[str]) -> int:
    digits_saved = 0
    for line in map(str.strip, lines):
        num = RomanNumeral(line)
        as_string = str(num)
        diff = len(line) - len(as_string)
        assert diff >= 0
        digits_saved += diff

    return digits_saved


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "numerals_file",
        default=os.path.join(os.path.dirname(__file__), "roman.txt"),
        nargs='?',
    )
    args = parser.parse_args()
    with open(args.numerals_file, mode="r") as f:
        print("Saved %d digits after transformation" % numerals_saved(f))
