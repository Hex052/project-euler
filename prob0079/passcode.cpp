#include <fstream>
#include <iomanip>
#include <iostream>
#include <set>

struct keylog {
	uint8_t first, second, third;

	bool operator==(const keylog &rhs) const {
		return this->first == rhs.first && this->second == rhs.second
		       && this->third == rhs.third;
	}
	bool operator!=(const keylog &rhs) const {
		return this->first != rhs.first || this->second != rhs.second
		       || this->third != rhs.third;
	}

	bool operator<(const keylog &rhs) const {
		if (this->first != rhs.first) {
			return this->first < rhs.first;
		}
		if (this->second != rhs.second) {
			return this->second < rhs.second;
		}
		if (this->third != rhs.third) {
			return this->third < rhs.third;
		}
		return false;
	}
	bool operator<=(const keylog &rhs) const {
		return *this < rhs || *this == rhs;
	}
	bool operator>(const keylog &rhs) const {
		return !(*this <= rhs);
	}
	bool operator>=(const keylog &rhs) const {
		return !(*this < rhs);
	}
	int as_int() const {
		return 100 * this->first + 10 * this->second + this->third;
	}
};


void load_file(const char *filename, std::set<keylog> &lines) {
	std::ifstream file(filename);
	std::string str;

	if (!file.good()) {
		std::cerr << "Failed to open file \"" << filename << '\"' << std::endl;
	}

	while (!file.eof()) {
		std::getline(file, str);
		auto size = str.size();
		if (size != 3) {
			if (size == 0) {
				return;
			}
			std::cerr << "Invalid line read! Expected 3 characters, got "
			          << str.size() << "!" << std::endl;
			exit(1);
		}
		lines.insert({str[0] - '0', str[1] - '0', str[2] - '0'});
	}
	return;
}

int main(int argc, char const *argv[]) {
	std::set<keylog> lines;
	load_file(argc != 2 ? "prob0079/keylog.txt" : argv[1], lines);

	std::cout << "All the sequences:\n";
	for (keylog i : lines) {
		std::cout << std::setw(3) << std::setfill('0') << i.as_int() << "\n";
	}

	return 0;
}
