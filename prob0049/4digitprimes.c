#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "../general/permutate.h"


// !!!!!!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!!!!!
//     THIS CODE DOES NOT WORK TO SOLVE PROBLEM 49
// !!!!!!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!!!!!


//! returns -1 on nothing found
int locate(char **arr, char *str, int len);
int distinctNums(char *str);

int main(int argc, char const *argv[]) {
	if (argc != 2) {
		fprintf(stderr, "Usage: %s prime_list\n", argv[0]);
		exit(1);
	}
	FILE *plist = fopen(argv[1], "r");
	char *line = NULL, str[5];
	size_t len, read;
	int index = 0, permindex = 0, foundpos;
	char **arr = malloc(1100 * sizeof(char*)), **perms = NULL;
	memset(arr, 0, sizeof(char*) * 1100);
	do {
		read = getline(&line, &len, plist);
	} while (read != 5);
	do {
		arr[index++] = line;
		line[4] = 0;
		line = NULL; len = 0;

		read = getline(&line, &len, plist);
	} while (read == 5);
	fclose(plist);

	perms = malloc(index * sizeof(char*));
	for (int i = 0; i < index; i++) {
		bool found = false;
		strcpy(str, arr[i]);
		while (permute(str, 4, 1) == -1) {
			if ((foundpos = locate(arr + i + 1, str, index - i)) != -1) {
				if (!found) {
					perms[permindex++] = arr[i];
					printf("\n%s", arr[i]);
					arr[i] = NULL;
					found = true;
				}
				perms[permindex++] = arr[i + 1 + foundpos];
				printf("%s", arr[i + 1 + foundpos]);
				arr[i + 1 + foundpos] = NULL;
			}
		}
	}
	return 0;
}

int locate(char **arr, char *str, int len) {
	for (int i = 0; len > 0; len--, i++, arr++) {
		if (*arr != NULL && strcmp(*arr, str) == 0) {
			return i;
		}
	}
	return -1;
}

int distinctNums(char *str) {
	char *ptr = str;
	int tot = 0;
	bool arr[10] = {0,0,0,0,0,0,0,0,0,0};
	while (*ptr != '\n' && *ptr != 0) {
		arr[*ptr - '0'] = true;
	}
	for (int i = 0; i < 10; i++) {
		if (arr[i])
			tot++;
	}
	return tot;
}
