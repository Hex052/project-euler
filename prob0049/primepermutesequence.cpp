#include <fstream>
#include <iostream>
#include <chrono>
#include <set>

#include "../general/permutestr.h"

//prototypes

//global variables
std::set<std::string> primes;
std::set<std::string>::iterator pbegin, pend;

int main(int argc, char const *argv[]) {
	auto start_time = std::chrono::high_resolution_clock::now();
	//Start stopwatch

	std::ifstream nplist("plist.txt");
	std::string str;
	while (!nplist.eof()) {
		nplist >> str;
		if (str.size()  == 4) {
			primes.insert(str);
		}
		else if (str.size() > 4) {
			break;
		}
	}
	nplist.close();
	pbegin = primes.begin();
	pend = primes.end();

	auto load_time = std::chrono::high_resolution_clock::now();
	auto nanos = std::chrono::duration_cast<std::chrono::nanoseconds>(load_time - start_time);
	std::cerr << "Load took " << nanos.count() << " ns\n\n";

	std::string copy;
	int val, data;
	std::set<int> v;
	for (std::string str : primes) {
		copy = str;
		val = std::stoi(str);
		v.insert(val);
		while (permutestr((char *)str.data(), 4)) {
			if (primes.count(str)) {
				data = std::stoi(str);
				v.insert(data);
				primes.erase(str);
			}
		}
		auto back = v.end();
		while (v.size() > 2) {
			auto front = v.begin();
			val = *front;
			front++;
			v.erase(v.begin()); //Why this worked and not v.erase(val) is beyond me
			do {
				data = (*front << 1) - val; // data = *front + (*front - val)
				if (v.count(data)) {
					std::cout << val << *front << data << '\n';
				}
				front++;
			} while(front != back);

		}
		v.clear();
	}

	//Stop stopwatch
	auto stop_time = std::chrono::high_resolution_clock::now();
	// nanos = std::chrono::duration_cast<std::chrono::nanoseconds>(stop_time - load_time);
	std::cerr << "\nSearch took " << std::chrono::duration_cast<std::chrono::nanoseconds>(stop_time - load_time).count() << " ns\n"
	// nanos = std::chrono::duration_cast<std::chrono::nanoseconds>(stop_time - start_time);
	"Execution took " << std::chrono::duration_cast<std::chrono::nanoseconds>(stop_time - start_time).count() << " ns\n";
	return 0;
}
