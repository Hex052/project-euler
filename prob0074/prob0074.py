import math
facts = {}
digits = []
appeared_already = []
total60 = 0

for i in range(0,10):
    facts[i] = math.factorial(i)

for n in range (0,1000000):
    appeared_already = []
    while True:
        num = 0
        digits = []
        if len(appeared_already)>0:
            m = appeared_already[len(appeared_already)-1]
        else:
            m = n
        while m > 0:
            digits.append(m%10)
            m = m//10
        for j in range(0,len(digits)):
            num += facts[digits[j]]
        if num in appeared_already:
            if len(appeared_already) == 59:
                total60 += 1
            break
        else:
            appeared_already.append(num)
print(total60)