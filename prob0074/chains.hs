module Main where

import Data.Array qualified
import Data.Bifunctor (second)
import Data.IntMap.Strict qualified
import Data.IntSet qualified
import Data.List qualified
import Data.Sequence (Seq ((:|>)), (|>))
import Data.Sequence qualified
import Data.Tree (Tree (Node), drawTree, rootLabel, subForest, unfoldForest, unfoldTree)
import System.IO (hPrint, stderr)

data Digits a = Digits a a a a a a a a a a
  deriving (Show)

instance (Ord a) => Ord (Digits a) where
  compare (Digits a0 a1 a2 a3 a4 a5 a6 a7 a8 a9) (Digits b0 b1 b2 b3 b4 b5 b6 b7 b8 b9) = compare (a0, a1, a2, a3, a4, a5, a6, a7, a8, a9) (b0, b1, b2, b3, b4, b5, b6, b7, b8, b9)

instance (Eq a) => Eq (Digits a) where
  (==) (Digits a0 a1 a2 a3 a4 a5 a6 a7 a8 a9) (Digits b0 b1 b2 b3 b4 b5 b6 b7 b8 b9) = a0 == b0 && a1 == b1 && a2 == b2 && a3 == b3 && a4 == b4 && a5 == b5 && a6 == b6 && a7 == b7 && a8 == b8 && a9 == b9
  (/=) (Digits a0 a1 a2 a3 a4 a5 a6 a7 a8 a9) (Digits b0 b1 b2 b3 b4 b5 b6 b7 b8 b9) = a0 /= b0 || a1 /= b1 || a2 /= b2 || a3 /= b3 || a4 /= b4 || a5 /= b5 || a6 /= b6 || a7 /= b7 || a8 /= b8 || a9 /= b9

zero :: Digits Int
zero = Digits 0 0 0 0 0 0 0 0 0 0

nextInChain' :: Digits Int -> Int
nextInChain' (Digits a0 a1 a2 a3 a4 a5 a6 a7 a8 a9) = a0 + a1 + 2 * a2 + 6 * a3 + 24 * a4 + 120 * a5 + 720 * a6 + 5040 * a7 + 40320 * a8 + 362880 * a9

nextInChain :: Int -> Int
nextInChain = nextInChain' . fromNumber

fromNumber :: Int -> Digits Int
fromNumber n = internal n zero
  where
    internal :: Int -> Digits Int -> Digits Int
    internal 0 a = a
    internal n (Digits a0 a1 a2 a3 a4 a5 a6 a7 a8 a9)
      | rem == 0 = internal quot $ Digits (a0 + 1) a1 a2 a3 a4 a5 a6 a7 a8 a9
      | rem == 1 = internal quot $ Digits a0 (a1 + 1) a2 a3 a4 a5 a6 a7 a8 a9
      | rem == 2 = internal quot $ Digits a0 a1 (a2 + 1) a3 a4 a5 a6 a7 a8 a9
      | rem == 3 = internal quot $ Digits a0 a1 a2 (a3 + 1) a4 a5 a6 a7 a8 a9
      | rem == 4 = internal quot $ Digits a0 a1 a2 a3 (a4 + 1) a5 a6 a7 a8 a9
      | rem == 5 = internal quot $ Digits a0 a1 a2 a3 a4 (a5 + 1) a6 a7 a8 a9
      | rem == 6 = internal quot $ Digits a0 a1 a2 a3 a4 a5 (a6 + 1) a7 a8 a9
      | rem == 7 = internal quot $ Digits a0 a1 a2 a3 a4 a5 a6 (a7 + 1) a8 a9
      | rem == 8 = internal quot $ Digits a0 a1 a2 a3 a4 a5 a6 a7 (a8 + 1) a9
      | rem == 9 = internal quot $ Digits a0 a1 a2 a3 a4 a5 a6 a7 a8 (a9 + 1)
      where
        (quot, rem) = quotRem n 10

main :: IO ()
main =
  let toPrev :: Data.IntMap.Strict.IntMap Data.IntSet.IntSet
      toPrev = Data.IntMap.Strict.fromListWith Data.IntSet.union $ map (\(a, b) -> (b, Data.IntSet.singleton a)) $ Data.Array.assocs belowMillion ++ Data.IntMap.Strict.assocs millionAndUp
      unfoldNext i = (i, maybe [] Data.IntSet.toList $ toPrev Data.IntMap.Strict.!? i)
      f (x, y) = (x, unfoldForest unfoldNext $ Data.List.delete y $ Data.IntSet.toList $ toPrev Data.IntMap.Strict.! x)
      startTree = map f [(2, 2), (145, 145), (169, 1454), (363601, 169), (1454, 363601), (871, 45361), (45361, 871), (872, 45362), (45362, 872), (40585, 40585)]
      -- forest = unfoldForest (\i -> (i, lookupNext i)) [2, 145, 169, {-363601, 1454,-} 871, {-45361,-} 872, {-45362,-} 40585]
      forest =
        map (uncurry Node) $
          take 2 startTree
            ++ [ (169, uncurry Node (second (uncurry Node (startTree !! 4) :) $ startTree !! 3) : snd (startTree !! 2)),
                 (363601, uncurry Node (second (uncurry Node (startTree !! 2) :) $ startTree !! 4) : snd (startTree !! 3)),
                 (1454, uncurry Node (second (uncurry Node (startTree !! 3) :) $ startTree !! 2) : snd (startTree !! 4)),
                 (871, uncurry Node (startTree !! 6) : snd (startTree !! 5)),
                 (45361, uncurry Node (startTree !! 5) : snd (startTree !! 6)),
                 (872, uncurry Node (startTree !! 8) : snd (startTree !! 7)),
                 (45362, uncurry Node (startTree !! 7) : snd (startTree !! 8))
               ]
            ++ [last startTree]
      -- add2 = map (uncurry Node . f) [(169, 1454), (363601, 169), (1454, 363601)]
      -- add1 = map (uncurry Node . f) [(871, 45361), (45361, 871), (872, 45362), (45362, 872)]
      -- add0 = map (uncurry Node . f) [(2, 2), (145, 145), (40585, 40585)]
      walkTree 60 _ = 1
      walkTree depth (Node _ sub) = foldr (\x acc -> walkTree (depth + 1) x + acc) 0 sub
      depth :: Tree a -> Data.IntMap.Strict.IntMap Int
      depth (Node _ []) = Data.IntMap.Strict.singleton 1 1
      depth (Node _ sub) = Data.IntMap.Strict.unionsWith (+) $ Data.IntMap.Strict.singleton 1 1 : map (Data.IntMap.Strict.mapKeys (+ 1) . depth) sub
      depths = map depth
   in do
        -- putStrLn $ drawTree $ unfoldTree (\x -> (show $ rootLabel x, subForest x)) $ forest !! 2
        hPrint stderr $ foldr (\x accum -> walkTree 1 x + accum) 0 forest

-- mapM_ (hPrint stderr . depth) forest

findLoops :: IO ()
-- findLoops = print (fromNumber 3) >> print (nextInChain 3) >> print belowMillion >> mapM_ putStrLn displayChains
findLoops = mapM_ putStrLn displayChains
  where
    displayChains :: [String]
    displayChains = map f separateChains
      where
        f seq = show (Data.Sequence.length seq) ++ ":\t" ++ foldr (\i s -> if null s then show i else show i ++ " -> " ++ s) "" seq

belowMillion :: Data.Array.Array Int Int
belowMillion = Data.Array.listArray (2, 999_999) $ map nextInChain [2 .. 999_999]

millionAndUp :: Data.IntMap.Strict.IntMap Int
millionAndUp = f Data.IntMap.Strict.empty $ filter (> 999_999) $ Data.Array.elems belowMillion
  where
    f result [] = result
    f result (x : xs)
      | next > 999_999 = f (Data.IntMap.Strict.insert x next result) (next : xs)
      | otherwise = f (Data.IntMap.Strict.insert x next result) xs
      where
        next = nextInChain x

lookupNext :: Int -> Int
lookupNext n
  | n > 999_999 = millionAndUp Data.IntMap.Strict.! n
  | otherwise = belowMillion Data.Array.! n

onlyChains :: Data.IntSet.IntSet
onlyChains = f $ Data.IntSet.fromList $ Data.IntMap.Strict.elems millionAndUp ++ Data.Array.elems belowMillion
  where
    f :: Data.IntSet.IntSet -> Data.IntSet.IntSet
    f prev
      | Data.IntSet.size next == Data.IntSet.size prev = prev
      | otherwise = f next
      where
        next = Data.IntSet.map lookupNext prev

separateChains :: [Data.Sequence.Seq Int]
separateChains = f' onlyChains
  where
    f' :: Data.IntSet.IntSet -> [Data.Sequence.Seq Int]
    f' set
      | Data.IntSet.null set = []
      | otherwise = f min (Data.Sequence.singleton min) min rest
      where
        (min, rest) = Data.IntSet.deleteFindMin set
    f :: Int -> Data.Sequence.Seq Int -> Int -> Data.IntSet.IntSet -> [Data.Sequence.Seq Int]
    f first seq last set
      | next == first = seq : f' set
      | otherwise = f first (seq |> next) next (Data.IntSet.delete next set)
      where
        next = lookupNext last
