module Main where

import Data.List (permutations)
import Data.Maybe (fromJust, isJust)

data MagicNGon a = Nodes [a] | Sides [(a, a, a)]
  deriving (Show)

instance (Show a, Num a, Ord a) => Eq (MagicNGon a) where
  left == right = EQ == compare left right

instance (Show a, Num a, Ord a) => Ord (MagicNGon a) where
  -- compare (Nodes a) (Nodes b) = compare (fromNodes a) (fromNodes b)
  -- compare a (Nodes b) = compare a $ fromNodes b
  -- compare (Nodes a) b = compare (fromNodes a) b
  -- compare left right
  --   | cmp /= EQ = cmp
  --   | otherwise = compare (concatGroups left) (concatGroups right)
  --   where
  --     magicLeft = magicNumber left
  --     magicRight = magicNumber right
  --     cmp = compare magicLeft magicRight
  compare left right = compare (concatGroups left) (concatGroups right)

concatGroups :: (Show a) => MagicNGon a -> String
concatGroups (Nodes a) = concatGroups $ fromNodes a
concatGroups (Sides a) = showList a
  where
    showThree (a1, a2, a3) = show a1 ++ show a2 ++ show a3
    showList [] = []
    showList (a : a's) = showThree a ++ showList a's

fromNodes :: [a] -> MagicNGon a
fromNodes nodes
  | odd nodeCount = error "number of nodes cannot be odd"
  | nodeCount < 6 = error "must be at least 6 nodes"
  | otherwise =
      let halves = splitAt (nodeCount `div` 2) nodes
          firstHalf = fst halves
          secondHalf = snd halves
       in Sides $ zip3 firstHalf secondHalf (tail secondHalf ++ [head secondHalf])
  where
    nodeCount = length nodes

isMinimal :: Ord a => MagicNGon a -> Bool
isMinimal (Nodes a) = isMinimal $ fromNodes a
isMinimal (Sides sides) =
  let (first, _, _) = head sides
      rest = tail sides
   in all (\(x, _, _) -> x > first) rest

magicNumber :: (Num a, Eq a) => MagicNGon a -> Maybe a
magicNumber (Nodes nodes) = magicNumber $ fromNodes nodes
magicNumber (Sides []) = Nothing
magicNumber (Sides [_]) = Nothing
magicNumber (Sides [_, _]) = Nothing
magicNumber (Sides sides)
  | allEqual = Just first
  | otherwise = Nothing
  where
    sums = map (\(a1, a2, a3) -> a1 + a2 + a3) sides
    first = head sums
    allEqual = all (== first) $ tail sums

-- isMagic = isJust . magicNumber
isMagic :: (Num a, Eq a) => MagicNGon a -> Bool
isMagic (Nodes nodes) = isMagic $ fromNodes nodes
isMagic (Sides []) = False
isMagic (Sides [_]) = False
isMagic (Sides [_, _]) = False
isMagic (Sides sides) = allEqual
  where
    sums = map (\(a1, a2, a3) -> a1 + a2 + a3) sides
    first = head sums
    allEqual = all (== first) $ tail sums

isValid :: MagicNGon a -> Bool
isValid (Nodes nodes) =
  let nodeCount = length nodes
   in nodeCount >= 6 && even nodeCount
isValid (Sides sides) = length sides >= 3

main :: IO ()
main =
  let magic5gons :: [MagicNGon Int]
      magic5gons =
        let possible = map fromNodes $ permutations [1 .. 10]
            minimals = filter isMinimal possible
            digits16 = filter (\(Sides s) -> any (\(x, _, _) -> x == 10) s) minimals
            magics = filter isMagic digits16
         in magics
      maxMagic = maximum magic5gons
      printNumber ngon = sequence_ [putStr $ show $ fromJust $ magicNumber ngon, putStr ": ", print ngon, putStr "  ", putStrLn $ concatGroups ngon]
   in do
        putStr "Tried "
        putStr $ show $ length magic5gons
        putStrLn " 5-gons:"
        printNumber $ head magic5gons
        printNumber maxMagic
