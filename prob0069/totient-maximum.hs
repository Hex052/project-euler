module Main where

import Data.List (intercalate)
import System.Environment (getArgs, getProgName)
import System.Exit (ExitCode (ExitFailure), exitWith)
import System.IO (IOMode (ReadMode), hGetContents, hPutStrLn, stderr, withFile)

mulPrimes endAt fileHandle = do
  contents <- hGetContents fileHandle
  let primes = map read $ lines contents
      solutions = takeWhile (<= endAt) $ scanl1 (*) primes
      problemSolution = last $ takeWhile (<= 1_000_000) $ scanl1 (*) primes
  putStr "All: "
  putStrLn $ intercalate ", " $ map show solutions
  putStr "Largest: "
  print $ last solutions
  putStr "Problem solution: "
  print problemSolution

usage = do
  name <- getProgName
  let messageLines =
        [ "usage: " ++ name ++ " <prime_list> <end_number>",
          "prime_list must be a file that contains prime numbers up to at least 1,000,000",
          "end_number must be at least 2",
          "",
          "This works by multiplying primes together until we reach the limit"
        ]
  hPutStrLn stderr $ unlines messageLines
  exitWith $ ExitFailure 1

main = do
  args <- getArgs
  let filename = head args
      endAt = read $ args !! 1
  if length args /= 2 || null filename || endAt < 2
    then usage
    else -- bracket (openFile filename ReadMode) hClose (runTotients endAt)
      withFile filename ReadMode (mulPrimes endAt) -- (runTotients endAt)

-- -- This is a bad solution, brute-force is almost always the worst way to go.
-- -- Thus it's commented out, but kept for posterity.
--
-- import Data.Ratio (Ratio, (%))
--
-- totient :: Int -> Int
-- totient n = (1 +) $ sum $ map (fromEnum . (== 1) . gcd n) [2 .. n - 1]
--
-- nOverTotient :: Int -> Ratio Int
-- nOverTotient n = n % totient n
--
-- largestNOverTotient ::
--   -- | List of candidates
--   [Int] ->
--   -- | List of primes, may be empty
--   [Int] ->
--   -- | Candidate for which the highest ratio was achieved and the ratio
--   (Int, Ratio Int)
-- largestNOverTotient [] _ = error "empty candidates"
-- largestNOverTotient (n : ns) ps
--   | null ps = internal ns n $ nOverTotient n
--   | otherwise = internal (removePrimes ns ps) n $ nOverTotient n
--   where
--     removePrimes [] _ = []
--     removePrimes ns [] = ns
--     removePrimes (n : ns) (p : ps)
--       | n == p = removePrimes ns ps
--       | otherwise = n : removePrimes ns (p : ps)
--     internal :: [Int] -> Int -> Ratio Int -> (Int, Ratio Int)
--     internal [] bigN nOver = (bigN, nOver)
--     internal (n : ns) bigN nOver
--       | try > nOver = internal ns n try
--       | otherwise = internal ns bigN nOver
--       where
--         try = nOverTotient n
--
-- runTotients endAt fileHandle = do
--   contents <- hGetContents fileHandle
--   let primes = map read $ lines contents :: [Int]
--       largest = largestNOverTotient [1 .. endAt] primes
--   putStr $ show $ fst largest
--   putStr ": "
--   print $ snd largest
