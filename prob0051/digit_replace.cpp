#include <errno.h>
#include <map>
#include <set>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>

void possible_patterns(std::set<std::string> &patterns, std::string &str,
                       std::string::iterator &begin,
                       const std::string::iterator &end, const char c,
                       int count_replaced = 0) {
	if (begin == end) {
		if (count_replaced != 0) {
			patterns.insert(str);
		}
		return;
	}
	if (*begin == c) {
		*begin = 'x';
		++begin;
		possible_patterns(patterns, str, begin, end, c, count_replaced + 1);
		--begin;
		*begin = c;
	}
	++begin;
	possible_patterns(patterns, str, begin, end, c, count_replaced);
	--begin;
}

std::set<std::string> possible_patterns(std::string str) {
	std::set<std::string> result;
	auto begin = str.begin();
	auto end = str.end();
	if (begin != end) {
		--end;
		// Moving end back because if we replace the last digit, we couldn't possibly
		// get enough prime numbers
		for (char c = '0'; c <= '9'; ++c) {
			possible_patterns(result, str, begin, end, c);
		}
	}
	return result;
}

int main(int argc, char const *argv[]) {
	const char *default_name = "./nplist.txt";
	FILE *nplist = NULL;
	if (argc == 2) {
		nplist = fopen(argv[1], "r");
	}
	else if (argc == 1) {
		nplist = fopen("./nplist.txt", "r");
	}
	else {
		fprintf(stderr,
		        "usage: %s [plist]\n"
		        "plist is a path to a file containing prime "
		        "numbers separated by newlines. Defaults to %s\n",
		        argv[0], default_name);
	}

	if (nplist == NULL) {
		int err = errno;
		fprintf(stderr, "Error %d opening %s : %s\n", err,
		        argc == 1 ? default_name : argv[1], strerror(err));
		exit(1);
	}

	const int length = 6;
	const int target_size = 8;

	char *line = NULL;
	size_t n = 0;
	ssize_t digits;
	do {
		digits = getdelim(&line, &n, '\n', nplist) - 1;
		if (digits < 0) {
			fprintf(stderr, "not enough prime numbers!\n");
			exit(2);
		}
	} while (digits < length);

	std::map<std::string, std::set<std::string>> patterns;

	do {
		digits = getdelim(&line, &n, '\n', nplist) - 1;
		if (digits < 0) {
			fprintf(stderr, "not enough prime numbers!\n");
			exit(2);
		}
		line[digits] = '\0';
		for (auto &pattern : possible_patterns(line)) {
			std::set<std::string> &elements = patterns[pattern];
			elements.insert(line);
			if (elements.size() >= target_size) {
				auto i = elements.begin();
				printf("Numbers: %s", i->c_str());
				++i;
				for (auto end = elements.end(); i != end; ++i) {
					printf(", %s", i->c_str());
				}
				printf("\nPattern: %s\n", pattern.c_str());
			}
		}
	} while (digits == length);

	return 0;
}
