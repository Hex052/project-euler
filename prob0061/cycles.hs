module Main where

import Data.Map qualified as Map
import Data.Set qualified as Set
import Debug.Trace (trace)

triangles = Set.fromList ["1035", "1081", "1128", "1176", "1225", "1275", "1326", "1378", "1431", "1485", "1540", "1596", "1653", "1711", "1770", "1830", "1891", "1953", "2016", "2080", "2145", "2211", "2278", "2346", "2415", "2485", "2556", "2628", "2701", "2775", "2850", "2926", "3003", "3081", "3160", "3240", "3321", "3403", "3486", "3570", "3655", "3741", "3828", "3916", "4005", "4095", "4186", "4278", "4371", "4465", "4560", "4656", "4753", "4851", "4950", "5050", "5151", "5253", "5356", "5460", "5565", "5671", "5778", "5886", "5995", "6105", "6216", "6328", "6441", "6555", "6670", "6786", "6903", "7021", "7140", "7260", "7381", "7503", "7626", "7750", "7875", "8001", "8128", "8256", "8385", "8515", "8646", "8778", "8911", "9045", "9180", "9316", "9453", "9591", "9730", "9870"]

squares = Set.fromList ["1024", "1089", "1156", "1225", "1296", "1369", "1444", "1521", "1600", "1681", "1764", "1849", "1936", "2025", "2116", "2209", "2304", "2401", "2500", "2601", "2704", "2809", "2916", "3025", "3136", "3249", "3364", "3481", "3600", "3721", "3844", "3969", "4096", "4225", "4356", "4489", "4624", "4761", "4900", "5041", "5184", "5329", "5476", "5625", "5776", "5929", "6084", "6241", "6400", "6561", "6724", "6889", "7056", "7225", "7396", "7569", "7744", "7921", "8100", "8281", "8464", "8649", "8836", "9025", "9216", "9409", "9604", "9801"]

pentagons = Set.fromList ["1001", "1080", "1162", "1247", "1335", "1426", "1520", "1617", "1717", "1820", "1926", "2035", "2147", "2262", "2380", "2501", "2625", "2752", "2882", "3015", "3151", "3290", "3432", "3577", "3725", "3876", "4030", "4187", "4347", "4510", "4676", "4845", "5017", "5192", "5370", "5551", "5735", "5922", "6112", "6305", "6501", "6700", "6902", "7107", "7315", "7526", "7740", "7957", "8177", "8400", "8626", "8855", "9087", "9322", "9560", "9801"]

hexagons = Set.fromList ["1035", "1128", "1225", "1326", "1431", "1540", "1653", "1770", "1891", "2016", "2145", "2278", "2415", "2556", "2701", "2850", "3003", "3160", "3321", "3486", "3655", "3828", "4005", "4186", "4371", "4560", "4753", "4950", "5151", "5356", "5565", "5778", "5995", "6216", "6441", "6670", "6903", "7140", "7381", "7626", "7875", "8128", "8385", "8646", "8911", "9180", "9453", "9730"]

heptagons = Set.fromList ["1053", "1134", "1218", "1305", "1395", "1488", "1584", "1683", "1785", "1890", "1998", "2109", "2223", "2340", "2460", "2583", "2709", "2838", "2970", "3105", "3243", "3384", "3528", "3675", "3825", "3978", "4134", "4293", "4455", "4620", "4788", "4959", "5133", "5310", "5490", "5673", "5859", "6048", "6240", "6435", "6633", "6834", "7038", "7245", "7455", "7668", "7884", "8103", "8325", "8550", "8778", "9009", "9243", "9480", "9720", "9963"]

octagons = Set.fromList ["1045", "1160", "1281", "1408", "1541", "1680", "1825", "1976", "2133", "2296", "2465", "2640", "2821", "3008", "3201", "3400", "3605", "3816", "4033", "4256", "4485", "4720", "4961", "5208", "5461", "5720", "5985", "6256", "6533", "6816", "7105", "7400", "7701", "8008", "8321", "8640", "8965", "9296", "9633", "9976"]

setCheck p q = map (\x -> (x, _filter $ drop 2 x)) p
  where
    _filter x = Set.filter ((x ==) . take 2) q
-- ^ setCheck :: [String] -> Set.Set String -> [(String, Set.Set String)]

-- | Does the same as setCheck but removes empty sets.
setCheckFilter p q = filter (Set.null . snd) $ setCheck p q
-- ^ setCheckFilter :: [String] -> Set.Set String -> [(String, Set.Set String)]

findLoop ::
  Ord a =>
  Map.Map a (Set.Set a) ->
  Map.Map a (Set.Set a) ->
  Map.Map a (Set.Set a) ->
  Map.Map a (Set.Set a) ->
  Map.Map a (Set.Set a) ->
  Map.Map a (Set.Set a) ->
  [a]
findLoop map1 map2 map3 map4 map5 map6
  | trace ("map1: " ++ show (Map.size map1)) $ Map.size map1 /= 1 = do
      -- Filter out the ones that aren't in the next set.
      let newMap = Map.map (Set.filter (`Map.member` map2)) map1
      let newMapWithoutEmpties = Map.filter Set.null newMap
      -- Then move once over and continue
      findLoop map6 newMapWithoutEmpties map2 map3 map4 map5
  | trace ("map6: " ++ show (Map.size map6)) $ Map.size map6 /= 1 = findLoop map6 map1 map2 map3 map4 map5
  | trace ("map5: " ++ show (Map.size map5)) $ Map.size map5 /= 1 = findLoop map5 map6 map1 map2 map3 map4
  | trace ("map4: " ++ show (Map.size map4)) $ Map.size map4 /= 1 = findLoop map4 map5 map6 map1 map2 map3
  | trace ("map3: " ++ show (Map.size map3)) $ Map.size map3 /= 1 = findLoop map3 map4 map5 map6 map1 map2
  | trace ("map2: " ++ show (Map.size map2)) $ Map.size map2 /= 1 = findLoop map2 map3 map4 map5 map6 map1
  -- All are of size 1 now
  -- \| otherwise = (head $ Map.keys map1, head $ Map.keys map2, head $ Map.keys map3, head $ Map.keys map4, head $ Map.keys map5, head $ Map.keys map6)
  | otherwise = map (head . Map.keys) [map1, map2, map3, map4, map5, map6]

main = do
  let mapTriangleSquare = Map.fromList $ setCheckFilter (Set.elems triangles) squares
  let mapSquarePentagon = Map.fromList $ setCheckFilter (Set.elems squares) pentagons
  let mapPentagonHexagon = Map.fromList $ setCheckFilter (Set.elems pentagons) hexagons
  let mapHexagonHeptagon = Map.fromList $ setCheckFilter (Set.elems hexagons) heptagons
  let mapHeptagonOctagon = Map.fromList $ setCheckFilter (Set.elems heptagons) octagons
  let mapOctagonTriangle = Map.fromList $ setCheckFilter (Set.elems octagons) triangles
  let results = findLoop mapTriangleSquare mapSquarePentagon mapPentagonHexagon mapHexagonHeptagon mapHeptagonOctagon mapOctagonTriangle
  let resultsLine = foldl (\x acc -> x ++ ' ' : acc) "" results
  let total = show $ sum $ map read results
  sequence_
    [ putStrLn $ init resultsLine,
      putStrLn total
    ]
