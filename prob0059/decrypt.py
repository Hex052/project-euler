#!/usr/bin/env python3
import itertools
import os

with open(os.path.join(os.path.dirname(__file__), "cipher.txt"), "r") as f:
    data = bytes(map(int, f.read().rstrip().split(",")))

for keys in itertools.product(tuple(map(ord, "abcdefghijklmnopqrstuvwxyz")), repeat=3):
    since = []
    decrypted = []
    for key_char, data_char in zip(itertools.cycle(keys), data):
        value_char = chr(key_char ^ data_char)
        if not value_char.isascii() or value_char in R"`}{|~":
            break
        decrypted.append(value_char)
    else:
        print("".join(decrypted))
        print("\nkey:", "".join(map(chr, keys)))
        print("sum:", sum(map(ord, decrypted)))
