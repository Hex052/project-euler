module Main where

import Data.List (intercalate)
import Data.Ratio (Ratio, denominator, numerator, (%))

noSquares :: Int -> [Int]
noSquares until = filter (not . isSquare) [2 .. until]
  where
    isSquare n = whole * whole == n
      where
        whole = floor $ sqrt $ fromIntegral n

isOdd :: Integral a => a -> Bool
isOdd = (/= 0) . (`rem` 2)

-- From
--  https://brilliant.org/wiki/quadratic-diophantine-equations-pells-equation/
--
-- Pell's equation always has nontrivial solutions. The fundamental solution is
--  x / y = { [ a0; a1, a2, ... , a(k-1) ] if k is even
--          { [ a0; a1, a2, ... , a(2k-1) ] if k is odd
--  The continued fractions are from sqrt(n), [ a0; (a1, a2, ... , a(k) )]

data ContinuedFraction = ContinuedFraction {whole :: Int, fractions :: [Int]}

instance Show ContinuedFraction where
  showsPrec p frac = showParen (p > 10) $ showString str
    where
      str = '[' : show (whole frac) ++ "; (" ++ intercalate ", " (map show $ fractions frac) ++ ")]"

sqrtParts :: Int -> ContinuedFraction
sqrtParts n
  | n == whole * whole = ContinuedFraction whole []
  | otherwise = ContinuedFraction whole $ inter 1 whole
  where
    whole = isqrt n
    isqrt = floor . sqrt . fromIntegral
    inter :: Int -> Int -> [Int]
    inter num denom =
      let ugly = (n - denom * denom) `div` num
          result = (whole + denom) `div` ugly
          nextDenom = denom - result * ugly
          return = result : next ugly (abs nextDenom)
          text = unwords $ map show [num, denom, result, ugly, nextDenom]
       in -- if ugly == 0 then trace (unwords $ map show [n, num, denom]) return else return
          return
    next :: Int -> Int -> [Int]
    next 1 _ = []
    next num denom = inter num denom

-- | Calculates the fraction that results from
convergentN ::
  Int ->
  -- | the number to take in total, for the continued part, one is subtracted from this
  ContinuedFraction ->
  Ratio Integer
convergentN n continued = foldr1 (\x accum -> x + recip accum) foldpart
  where
    firstN n continued = take n $ whole continued : cycle (fractions continued)
    selection = firstN n continued
    foldpart = map ((% 1) . toInteger) selection

-- copied from the preivous
generateXY :: Int -> (Integer, Integer)
generateXY d = (numerator frac, denominator frac)
  where
    continued = sqrtParts d
    k = length $ fractions continued
    frac = convergentN (if isOdd k then 2 * k else k) continued

putDXY (d, x, y) = sequence_ $ map putStrLn ["  d: " ++ show d, "  x: " ++ show x, "  y: " ++ show y]

main :: IO ()
main =
  let ds = noSquares 1000
      count = length ds
      -- f d = (d, numerator frac, denominator frac)
      --   where
      --     continued = sqrtParts d
      --     k = length $ fractions continued
      --     frac = convergentN (if isOdd k then 2 * k else k) continued
      xys :: [(Integer, Integer)]
      xys = map generateXY ds
      xs = map fst xys
      dxys = uncurry (zip3 ds) $ unzip xys
      largest = foldl1 f dxys
        where
          f (ad, ax, ay) (bd, bx, by) = if ax > bx then (ad, ax, ay) else (bd, bx, by)
      sqrt61 = sqrtParts 61
   in do
        -- To check,this, I tested with sqrt61.
        -- print sqrt61
        -- print $ convergentN 22 sqrt61
        -- print $ length $ fractions sqrt61
        -- print $ generateXY 61
        -- print $ take 22 $ whole sqrt61 : cycle (fractions sqrt61)

        putStr ("x's and y's (" ++ show count ++ "): ")
        -- hFlush stdout
        -- print xys
        print $ zip ds xys
        putStrLn "maximum:"
        -- print $ maximum xs
        putDXY largest

-- import Math.NumberTheory.Roots (exactSquareRoot, isSquare)
--
-- noSquares until = filter (not . isSquare) [2 .. until]
--
-- badMinimal :: Integral a => a -> a -> (a, a)
-- badMinimal startXAt d
--   | startXAt < 2 = f 2 4
--   | otherwise = f startXAt (startXAt * startXAt)
--   where
--     f x xSquare
--       | snd div /= 0 = next
--       | otherwise = maybe next (x,) sqrt
--       where
--         next = f (x + 1) (xSquare + 2 * x + 1)
--         div = (xSquare - 1) `quotRem` d
--         sqrt = exactSquareRoot $ fst div
-- -- ^ Returns (x,y) for the diophantine
--
-- badMinimal1 = badMinimal 2
--
-- -- This is a bad method for doing this. It brute-forces the solution.
