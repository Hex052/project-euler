module Main where

import Data.List (intercalate)
import Data.Ratio (denominator, numerator, (%))
import Data.Ratio qualified

data ContinuedFraction = ContinuedFraction {whole :: Int, fractions :: [Int]}

instance Show ContinuedFraction where
  showsPrec p frac = showParen (p > 10) $ showString str
    where
      str = '[' : show (whole frac) ++ "; (" ++ intercalate ", " (map show $ fractions frac) ++ ")]"

eContinued = ContinuedFraction 2 $ values 2
  where
    values n = 1 : n : 1 : values (n + 2)

root2Continued = ContinuedFraction 1 $ repeat 2

main :: IO ()
main =
  let firstN n continued = take n $ whole continued : fractions continued
      convergentN n continued = foldr1 (\x accum -> x + (denominator accum % numerator accum)) foldpart
        where
          selection = firstN n continued
          foldpart = map ((% big1) . toInteger) selection
          big1 = toInteger 1
      convergent10Sqrt2 = convergentN 10 root2Continued
      convergent100E = convergentN 100 eContinued
   in do
        putStr "10th convergent of sqrt(2): "
        print convergent10Sqrt2
        putStr "100th convergent of e: "
        print convergent100E
        putStr "Sum of digits in numerator: "
        print $ sum $ map (subtract (fromEnum '0') . fromEnum) $ show $ numerator convergent100E
