module Main where

import Data.Ratio (Ratio, denominator, numerator, (%))

changeBase orig newBase = ((numerator orig * newBase) `quot` denominator orig) % newBase

main = print $ maximum $ filter (< 3 % 7) $ map (changeBase $ 3 % 7) [8 .. 1_000_000]
