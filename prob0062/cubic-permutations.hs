module Main where

import Data.List (sort)
import Data.Map qualified
import Data.Maybe (catMaybes, fromMaybe, listToMaybe, mapMaybe)
import Data.Set qualified

cubesChangeAt :: [[String]]

-- | Sorted largest to smallest
cubesChangeAt =
  let cubes = map (show . (^ 3)) [346 ..]
      nextCubes :: [String] -> [String] -> [[String]]
      nextCubes (n : ns) [] = nextCubes ns [n]
      nextCubes (n : ns) (same : rest) = if length same == length n then nextCubes ns (n : same : rest) else (same : rest) : nextCubes ns [n]
   in nextCubes (tail cubes) [head cubes]

main =
  let groups = filter ((0 /=) . Data.Map.size) $ map (Data.Map.filter ((5 ==) . length) . Data.Map.fromListWith (++) . map (\y -> (sort y, [y]))) cubesChangeAt
      first = head groups
      least = minimum $ Data.Map.foldr (++) [] first
   in do
        print first
        putStrLn least
