#include <stdio.h>
#include <gmp.h>

int main(int argc, char const *argv[]) {
	mpz_t prime, out;
	mpz_init(out);
	mpz_init_set_ui(prime, 1);
	mpz_mul_2exp(out, prime, 7830457);
	mpz_mul_ui(prime, out, 28433);
	mpz_swap(prime,out);
	mpz_add_ui(prime,out,1);

	printf("%lu\n", mpz_fdiv_r_ui(out, prime, 10000000000));
	return 0;
}
